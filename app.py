import os
from alyce import Alyce
from flask_ngrok import run_with_ngrok
from flask import Flask, request, render_template

app = Flask(__name__, static_url_path='', static_folder=(os.getcwd()+'/public'))
run_with_ngrok(app)

@app.route('/', methods=['GET', 'POST'])
def index():
	if(request.method == "POST"):
		story = request.form.get("story")
		if(story == None):
			return "Please Enter a Story"
		else:
			return Alyce.processStory(story.split("\n"))
	else:
		return render_template('index.html')

if __name__ == '__main__':
	app.run(debug=True, port=3000, host="0.0.0.0")

def startFromColab():
	app.run()