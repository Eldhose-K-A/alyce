import spacy
import neuralcoref
import sys
import re

nlp = spacy.load('en')
coref = neuralcoref.NeuralCoref(nlp.vocab)
nlp.add_pipe(coref, name='neuralcoref')

def find_and_replace_dialogues(input_string):
	dialogues = re.findall(r'\"(.+?)\"', input_string)
	dialogue_count = 0
	for dialogue in dialogues:
		input_string = input_string.replace(dialogue,"<-DIALOGUE-"+str(dialogue_count)+"->")
		dialogue_count = dialogue_count + 1
	return input_string, dialogues

def processStoryBackup(sentences):
	sentences, dialogues = find_and_replace_dialogues("<JOIN>".join(sentences))
	sentences = sentences.split("<JOIN>")

	dialogue_count = len(dialogues)
	dialogue_index = 0

	new_sentences = []
	add = []

	for sentence in sentences:
		doc = nlp(sentence)
		resolved_string = doc._.coref_resolved
		add.append(resolved_string)
		dialogue_spaces = re.findall(r'<-DIALOGUE-[0-9][0-9]*->',resolved_string)
		for dialogue_space in dialogue_spaces:
			if(dialogue_index < dialogue_count):
				resolved_string = resolved_string.replace(dialogue_space,"<b><span style='color: blue;'>\'"+dialogues[dialogue_index]+"\'</span></b>")
				dialogue_index = dialogue_index + 1
		new_sentences.append(resolved_string+"\n")
	response_string = ("".join(new_sentences)).replace("\n","<br/>").replace("\"","")
	#response_string = "<br/>### ".join(add)
	return response_string

def processStory(paragraphs):
	paragraphs, dialogues = find_and_replace_dialogues("<JOIN>".join(paragraphs))
	paragraphs = paragraphs.split("<JOIN>")
	story_list = []

	for sentences in paragraphs:
		sentences = sentences.split(".")
		if not (len(sentences)==1 and sentences[0]==''):
			story_list.append(sentences.copy())

	dialogue_count = len(dialogues)
	dialogue_index = 0
	response_string = ""

	for j in range(len(story_list)):
		paragraph = story_list[j]
		for i in range(len(paragraph)):
			if(paragraph[i].find("<-DIALOGUE-")!=-1):
				words = paragraph[i].upper().split(" ")
				if("HE" in words or "SHE" in words):
					if( i > 0 ):
						doc = nlp(".".join([paragraph[i-1],paragraph[i]]))
						response_string = response_string + "<br/><br/>" + doc._.coref_resolved
						sentences = doc._.coref_resolved.split(".")
						paragraph[i] = sentences[-1]
					elif( j > 0 ):
						doc = nlp(".".join(story_list[j-1])+"."+paragraph[i])
						response_string = response_string + "<br/><br/>" + doc._.coref_resolved
						sentences = doc._.coref_resolved.split(".")
						paragraph[i] = sentences[-1]
	
	for i in range(len(story_list)):
		story_list[i] = ".".join(story_list[i])
		dialogue_spaces = re.findall(r'<-DIALOGUE-[0-9][0-9]*->',story_list[i])
		for dialogue_space in dialogue_spaces:
			if(dialogue_index < dialogue_count):
				story_list[i] = story_list[i].replace(dialogue_space,"<b><span style='color: blue;'>"+dialogues[dialogue_index]+"</span></b>")
				dialogue_index = dialogue_index + 1

	response_string = "<br/><br/>".join(story_list)

	return response_string


